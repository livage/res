<?php 
session_start();
require('includes/config.php');
?>
<!DOCTYPE html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">
<link rel="stylesheet" href="css/nivo-slider.css" media="screen">
 <link rel="stylesheet" href="css/animations.css" media="screen">

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">

	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Search
================================================== -->
<section class="search margin-bottom-50">

</section>



<!-- Content
================================================== -->
<div class="container">
	<div class="row fullwidth-layout">

		<div class="col-md-12">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-15">

				<div class="col-md-6">
					<!-- Sort by -->
					<div class="sort-by">
						<h1>About Us</hl>

						
					</div>
				</div>

				<div class="col-md-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						
					</div>
				</div>
			</div>

			
			<!-- Listings -->
			<div class="listings-container list-layout">


<div class="slider-wrapper theme-default cushycms">
               <div id="nivoslider" class="nivoSlider">
                  <a href="#"><img src="images/pages.jpg" ></a>
                   <a href="#"><img src="images/pages2.jpg" ></a>
                  <a href="#"><img src="images/pages3.jpg"></a>
               </div>
            </div>
            
            
            
            
            
<h3 class="cushycms">Vision</h3>
<p class="cushycms">To become the prime and most preferred real estate consulting company in all the markets we serve.  </p>
<h3 class="cushycms">Mission</h3>
<p class="cushycms">To be the leaders in the property market through excellent service and provision of a diverse range of realty products and services to the satisfaction of all our stakeholders</p>
<h3 class="cushycms">Our Values</h3>
<p class="cushycms">• Integrity<br>
• Professionalism<br>
• Excellence<br>
• Honesty<br>
• Trust<br>
• Creativity<br>
• Teamwork</p>

<h3 class="cushycms">We Serve:</h3>
<p class="cushycms">• Businesses and individuals looking for buyers of their property:<br>
• Businesses and individuals looking for properties to buy;<br>
• Businesses intending to find offices to lease in the CBD or in other areas;<br>
• Individuals looking for accommodation on term lease basis;<br>
• Property owners looking for property managers and consultants to administer their properties and offer them hassle free maintenance and
payment schedules.<br>
• Organisations in need of property valuations for capital transactions or for balance sheet extractions;<br>
• Property developers looking for project managers.<br>
• Individuals or organisations looking for auctioneers;<br><br>
We provide superior personal services to buyers and sellers. We manage
properties professionally and ensure that clients get a good return on their investments. </p>

			</div>
			<!-- Listings Container / End -->

			<div class="clearfix"></div>
			<!-- Pagination -->
			
			<!-- Pagination / End -->

		</div>

	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
 <script src="js/jquery.nivo.slider.pack.js"></script>
  <script src="js/imagesloaded.pkgd.min.js"></script>
  <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.js"></script>
      <script src="js/jquery.parallax.js"></script>
      <script src="js/jquery.wait.js"></script> 
      <script src="js/modernizr-2.6.2.min.js"></script> 
      <script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
      <script src="js/jquery.nivo.slider.pack.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/superfish.js"></script>
      <script src="js/tweetMachine.js"></script>
      <script src="js/tytabs.js"></script>
      <script src="js/jquery.gmap.min.js"></script>
      <script src="js/circularnav.js"></script>
      <script src="js/jquery.sticky.js"></script>
      <script src="js/jflickrfeed.js"></script>
      <script src="js/imagesloaded.pkgd.min.js"></script>
      <script src="js/waypoints.min.js"></script>
      <script src="js/spectrum.js"></script>
      <script src="js/switcher.js"></script>
      <script src="js/custom.js"></script>


<!-- Style Switcher
================================================== -->
<script src="scripts/switcher.js"></script>


<!-- Style Switcher / End -->


</div>
<!-- Wrapper / End -->


</body>
</html>