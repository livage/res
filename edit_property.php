<?php session_start();
require('includes/config.php');
?>
<!DOCTYPE html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->

	<!-- Scripts
================================================== -->
	<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
	<script type="text/javascript" src="scripts/chosen.min.js"></script>
	<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
	<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
	<script type="text/javascript" src="scripts/rangeSlider.js"></script>
	<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
	<script type="text/javascript" src="scripts/slick.min.js"></script>
	<script type="text/javascript" src="scripts/masonry.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
	<script type="text/javascript" src="scripts/tooltips.min.js"></script>
	<script type="text/javascript" src="scripts/custom.js"></script>


	<!-- DropZone | Documentation: http://dropzonejs.com -->
	<script type="text/javascript" src="scripts/dropzone.js"></script>
	<script>
		$(".dropzone").dropzone({
			dictDefaultMessage: "<i class='sl sl-icon-plus'></i> Click here or drop files to upload",
		});
	</script>


	<!-- Style Switcher
    ================================================== -->
	<script src="scripts/switcher.js"></script>


	<!-- Style Switcher / End -->

<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="jquery.js"></script>
<script type='text/javascript' src='jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />

<script type="text/javascript">
		$().ready(function() {
			$("#region").autocomplete("get_course_sub.php", {

				width: 260,
				matchContains: true,
				mustMatch: true,
				minChars: 0,
				//multiple: true,
				highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});
			$("#suburb").autocomplete("get_course_list.php" , {
				extraParams: {
					region:function() { return $('#region').val();}
				},
				width: 260,
				matchContains: true,
				mustMatch: true,
				minChars: 0,
			//	multiple: true,
				highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});
		});
	</script>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">

	<!-- Header -->
	<?php
							include("includes/header.php");


						?>


	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== --><!-- Content
================================================== -->
<div class="container">
<div class="row">


<?php
require('includes/config.php');

$query = "SELECT * FROM book inner join subcat on b_subcat = subcat_id INNER JOIN category ON cat_id = parent_id  where b_id=".$_GET['id'];
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_assoc($result);



//var_dump($row);
?>


	<form action='process_addproperty.php' method='POST' enctype="multipart/form-data" autocomplete="off">
	  
	<!-- Submit Page -->
	<div class="col-md-4">

				
						<br><b>Property Title:</b><br>
						<input type='text' name='name' size='40' value="<?php echo $row['b_nm'];?>">
						<br><br>
						</div>
                        
                        
                        <br><div class="col-md-4">
						<b>Status:</b><br>
						<select  name="cat">
								<?php
									
										require('includes/config.php');
			
											$query="select * from category ";
			
											$res=mysqli_query($conn,$query);
											
											while($row=mysqli_fetch_assoc($res))
											{
												echo "<option disabled>".$row['cat_nm'];
												
												$q2 = "select * from subcat where parent_id = ".$row['cat_id'];
												
												$res2 = mysqli_query($conn,$q2) or die("Can't Execute Query..");
												while($row2 = mysqli_fetch_assoc($res2))
												{	
												
										echo '<option value="'.$row2['subcat_id'].'"> - '.$row2['subcat_nm'];
												
													
												}
												
											}
										//	mysqli_close($link);
								?>
						</select>
						<br><br></div>
                        
                        <div class="col-md-4">
						<?php
require('includes/config.php');

$query = "SELECT * FROM book inner join subcat on b_subcat = subcat_id INNER JOIN category ON cat_id = parent_id  where b_id=".$_GET['id'];
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_assoc($result);



//var_dump($row);
?>
						<b>Description:</b><br>
                        
                        <textarea cols="40" rows="2" name='description'> <?php echo $row['b_desc'];?></textarea>
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						<b>Location: (e.g Harare North / Harare East etc)</b><br>
						<input type='text' name='publisher' size='40' id='region' value="<?php echo $row['b_publisher'];?>">
						<br><br></div>
                        
                        
                        <div class="col-md-4">
						
						<b>Suburb:</b><br>
						<input type='text' name='suburb' size='40' id='suburb' value="<?php echo $row['b_edition'];?>">
						<br><br></div>

						<div class="col-md-4">

						<b>Size: (Sq/m)</b><br>
						<input type='text' name='area' size='40' placeholder="sq m" value="<?php echo $row['b_area'];?>">
						<br><br></div>

                        <div class="col-md-4">
						
						<b>Number of Bedrooms:</b><br>
						<input type='text' name='area' size='40' placeholder="sq m" value="<?php echo $row['b_isbn'];?>">
						<br><br></div>
                        
                        <div class="col-md-4">
						
						<b>Number of Bathrooms:</b><br>
						<input type='text' name='area' size='40' placeholder="sq m" value="<?php echo $row['b_page'];?>">
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						<b>Price:</b><br>
						<input type='text' name='price' size='40' data-unit='USD' value="<?php echo $row['b_area'];?>">
						<br><br></div>
              
               <div class="col-md-4">
					<!-- Avatar -->
					<div class="edit-profile-photo">
						<?php echo $row['b_id'].'-1-'.$row['b_img'];?> 
						<div class="change-photo-btn">
							<div class="photoUpload">
							    <span><i class="fa fa-upload"></i> Upload Photo</span>
							    <input type="file" class="upload" />
							</div>
						</div>
					</div>

				</div>
                        
                        <div class="col-md-4">
						
						<b>Image 2:</b><br>
						<input type='file' name='img2'  size='35' multiple>
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						
						<b>Image 3:</b><br>
						<input type='file' name='img3'  size='35' multiple>
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						
						<b>Image 4:</b><br>
						<input type='file' name='img4'  size='35' multiple>
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						
						<b>Image 5:</b><br>
						<input type='file' name='img5'  size='35' multiple>
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						
						<b>Image 6:</b><br>
						<input type='file' name='img6'  size='35' multiple>
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						
						<b>Image 7:</b><br>
						<input type='file' name='img7'  size='35' multiple>
						<br><br>
						</div>
                        
                        <div class="col-md-4">
						
						<b>Image 8:</b><br>
						<input type='file' name='img8'  size='35' multiple>
						<br><br>
                        </div>
              
              
       	<!-- Checkboxes -->
    <div class="col-md-4">
    <a href="#" class="more-search-options-trigger margin-bottom-10 margin-top-30" data-open-title="Additional Features" data-close-title="Additional Features"></a>

					<div class="more-search-options relative">

						<!-- Checkboxes -->
						<div class="checkboxes one-in-row margin-bottom-10">

		<input id="check-2" type="checkbox" name="ac" value="1">
		<label for="check-2">Air Conditioning</label>

		<input id="check-3" type="checkbox" name="sp" value="1">
		<label for="check-3">Swimming Pool</label>

		

		<input id="check-5" type="checkbox" name="lr" value="1">
		<label for="check-5">Laundry Room</label>


		<input id="check-6" type="checkbox" name="gym" value="1">
		<label for="check-6">Gym</label>

	

	</div>
						<!-- Checkboxes / End -->

					</div>
    
    </div>
    
    
    
    
    
    
    
    
    
	
	<!-- Checkboxes / End -->

                           <div class="col-md-6">
						<input class="button preview margin-top-5"  type='submit'  value='   Update  '  >
						   </div>
				</form>
			</div>
	</div>
<!--
</div>
</div>
 Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>





</div>
<!-- Wrapper / End -->


</body>
</html>