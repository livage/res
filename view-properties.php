<?php 
session_start();
require('includes/config.php');

	$q="select * from book";
	 $res=mysqli_query($conn,$q) or die("Can't Execute Query...");
	?>
<!DOCTYPE html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">

	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== -->



<!-- Content
================================================== -->
<div class="container">
<div class="row">

	<!-- Submit Page -->
	<div class="col-md-6">
				
                <br><br><br>
        <div class="plan featured">
				
					<table width="1400px" class="basic-table" align="center">
						
						<tr>
<th width="93">No:</th>
<th width="108">Title</th>
<th width="123">Location</th>
<th width="132">Bedrooms</th>

<th width="127">View </th>
<th width="127">Edit</th>
<th width="130">Activate</th>		
<th width="124">Delete</th>				
							
						</tr>
						<?php
						
						require('includes/config.php');


						$q="select * from book where b_user='". $_SESSION['unm']."'";

	 $res=mysqli_query($conn,$q) or die("Can't Execute Query...");

							$count=1;
							while($row=mysqli_fetch_assoc($res))
							{
							echo '<tr>
										<td>'.$count.'
										<td align="left">'.$row['b_nm'].'
										<td align="left">'.$row['b_publisher'].'
										<td align="left">'.$row['b_isbn'];
										
				echo 	'<td><a href="single-property-page.php?id='. $row['b_id'].'"><img src="images/view2.png" ></a>';
										
										echo 	'<td><a href="edit_property.php?id='.$row['b_id'].'"><img src="images/edit.png" ></a>';
										echo 	'<td><a href="#"><img src="images/activate.png" ></a>';
										
									echo 	'<td><a href="process_del_book.php?id='.$row['b_id'].'"><img src="images/drop.png" ></a>
												
									
									</tr>';
									$count++;
							}
						?>

					</TABLE>
				
			</div><br><br>
                <p>
				<a href="submit-property.php" class="button">Add Another Property</a>
               
				
		</p>
                
                
                
			</div>
	</div>

</div>
</div>
<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>


<!-- DropZone | Documentation: http://dropzonejs.com -->
<script type="text/javascript" src="scripts/dropzone.js"></script>
<script>
	$(".dropzone").dropzone({
		dictDefaultMessage: "<i class='sl sl-icon-plus'></i> Click here or drop files to upload",
	});
</script>


<!-- Style Switcher
================================================== -->
<script src="scripts/switcher.js"></script>


<!-- Style Switcher / End -->


</div>
<!-- Wrapper / End -->


</body>
</html>