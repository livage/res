<?php session_start();
require('includes/config.php');
?>
<!DOCTYPE html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->

	<!-- Scripts
================================================== -->
	<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>


<!-- DropZone | Documentation: http://dropzonejs.com -->
<script type="text/javascript" src="scripts/dropzone.js"></script>
<script>
	$(".dropzone").dropzone({
		dictDefaultMessage: "<i class='sl sl-icon-plus'></i> Click here or drop files to upload",
	});
</script>


	<!-- Style Switcher
    ================================================== -->
	<script src="scripts/switcher.js"></script>


	<!-- Style Switcher / End -->

<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="jquery.js"></script>
<script type='text/javascript' src='jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />

<script type="text/javascript">
		$().ready(function() {
			$("#region").autocomplete("get_course_sub.php", {

				width: 260,
				matchContains: true,
				mustMatch: true,
				minChars: 0,
				//multiple: true,
				highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});
			$("#suburb").autocomplete("get_course_list.php" , {
				extraParams: {
					region:function() { return $('#region').val();}
				},
				width: 260,
				matchContains: true,
				mustMatch: true,
				minChars: 0,
			//	multiple: true,
				highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});
		});
	</script>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">

	<!-- Header -->
	<?php
							include("includes/header.php");


						?>


	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== -->



<!-- Content
================================================== -->
<div class="container">
<div class="row">

<form action='process_addproperty.php' method='POST' enctype="multipart/form-data" autocomplete="off">


	<!-- Submit Page -->
	<div class="col-md-12">
		<div class="submit-page"><!-- Section -->
		<h3>Basic Information</h3>
		<div class="submit-section">

			<!-- Title -->
			<div class="form">
				<h5>Property Title <i class="tip" data-tip-content="Type title that will also contains an unique feature of your property (e.g. renovated, air contidioned)"></i></h5>
				<input type='text' name='name'/>
			</div>

			<!-- Row -->
			<div class="row with-forms">

				<!-- Status -->
				<div class="col-md-6">
					<h5>Status</h5>
					<select  name="cat">
								<?php
									
										require('includes/config.php');
			
											$query="select * from category ";
			
											$res=mysqli_query($conn,$query);
											
											while($row=mysqli_fetch_assoc($res))
											{
												echo "<option disabled>".$row['cat_nm'];
												
												$q2 = "select * from subcat where parent_id = ".$row['cat_id'];
												
												$res2 = mysqli_query($conn,$q2) or die("Can't Execute Query..");
												while($row2 = mysqli_fetch_assoc($res2))
												{	
												
										echo '<option value="'.$row2['subcat_id'].'"> - '.$row2['subcat_nm'];
												
													
												}
												
											}
										//	mysqli_close($link);
								?>
						</select>
				</div>

				<!-- Type -->
				<div class="col-md-6">
					<h5>Price <i class="tip" data-tip-content="Type overall or monthly price if property is for rent"></i></h5>
					<div class="select-input disabled-first-option">
						<input type='text' name='price' data-unit='USD' >
					</div>
				</div>

			</div>
			<!-- Row / End -->


			<!-- Row -->
			<div class="row with-forms">

				<!-- Price -->
				
				
				<!-- Area -->
				<div class="col-md-6">
					<h5>Area</h5>
					<div class="select-input disabled-first-option">
						<input type='text' name='area' data-unit="Sq ms">
					</div>
				</div>

				<!-- Rooms -->			
				<div class="col-md-6">
					<h5>Bedrooms</h5>
<select class="chosen-select-no-single" name="isbn" >
						<option label="blank"></option>	
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="More">More than 5</option>
					</select>				</div>

			</div>
			<!-- Row / End -->

		</div>
		<!-- Section / End -->

		<!-- Section -->
		<h3>Location</h3>
		<div class="submit-section">

			<!-- Row -->
			<div class="row with-forms">

				<!-- Address -->
				<div class="col-md-6">
					<h5>Address</h5>
					<input type="text">
				</div>

				<!-- City -->
				<div class="col-md-6">
					<h5>City</h5>
					<input type="text">
				</div>

				<!-- City -->
				<div class="col-md-6">
					<h5>Region/Province: (e.g Harare North / Harare East etc</h5>
					<input type='text' name='publisher' size='40' id='region'>
				</div>

				<!-- Zip-Code -->
				<div class="col-md-6">
					<h5>Suburb</h5>
					<input type='text' name='suburb' size='40' id='suburb'>
				</div>

			</div>
			<!-- Row / End -->

		</div>
		<!-- Section / End -->


		<!-- Section -->
		<h3>Detailed Information</h3>
		<div class="submit-section">

			<!-- Description -->
			<div class="form">
				<h5>Description</h5>
				<textarea class="WYSIWYG" name='description' cols="40" rows="3" spellcheck="true"></textarea>
			</div>

			<!-- Row -->
			<div class="row with-forms">

				<!-- Baths -->
				<div class="col-md-6">
					<h5>Bathrooms <span>(optional)</span></h5>
					<select class="chosen-select-no-single" name="pages" >
						<option label="blank" ></option>	
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="More">More than 5</option>
					</select>
				</div>
                
                
                <div class="col-md-6">
    <a href="#" class="more-search-options-trigger margin-bottom-10 margin-top-30" data-open-title="Additional Features" data-close-title="Additional Features"></a>

					<div class="more-search-options relative">

						<!-- Checkboxes -->
						<div class="checkboxes one-in-row margin-bottom-10">

		<input id="check-2" type="checkbox" name="ac" value="1">
		<label for="check-2">Air Conditioning</label>

		<input id="check-3" type="checkbox" name="sp" value="1">
		<label for="check-3">Swimming Pool</label>

		<input id="check-4" type="checkbox" name="ch" value="1">
		<label for="check-4">Central Heating</label>

		<input id="check-5" type="checkbox" name="lr" value="1">
		<label for="check-5">Laundry Room</label>


		<input id="check-6" type="checkbox" name="gym" value="1">
		<label for="check-6">Gym</label>

		<input id="check-7" type="checkbox" name="al" value="1">
		<label for="check-7">Alarm</label>

		<input id="check-8" type="checkbox" name="wc" value="1">
		<label for="check-8">Window Covering</label>

	</div>
						<!-- Checkboxes / End -->

					</div>
    
    </div>

			</div>
			<!-- Row / End -->

		</div>
		<!-- Section / End -->


		<!-- Section -->
		<h3>Contact Details</h3>
		<div class="submit-section">

			<!-- Row -->
			<div class="row with-forms">


				<!-- Email -->
				<div class="col-md-6">
					<h5>E-Mail</h5>
					<input type="text">
				</div>

				<!-- Name -->
				<div class="col-md-6">
					<h5>Phone <span>(optional)</span></h5>
					<input type="text">
				</div>

			</div>
			<!-- Row / End -->

		</div>
		<!-- Section / End -->
		<div class="col-md-6">
						<input class="button preview margin-top-5"  type='submit'  value='   OK   '  >
						   </div>

		</div>
	</div>

</div>

</form>
	<h3>Gallery</h3>
	<div class="submit-section">
		<form action="upload_image" class="dropzone" ></form>
	</div>

<!-- Section -->

		<!-- Section / End -->
</div>
	</div>
<!--
</div>
</div>
 Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>





</div>
<!-- Wrapper / End -->





<script type="text/javascript" src="scripts/dropzone.js"></script>
<script>
	$(".dropzone").dropzone({
		dictDefaultMessage: "<i class='sl sl-icon-plus'></i> Click here or drop files to upload",
	});
</script>

</body>
</html>