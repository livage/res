<?php 
session_start();
require('includes/config.php');
?>

<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">



	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						
						<li>My Profile</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">


		<!-- Widget -->
        <?php
		
		
require('includes/config.php');

	if(!(isset($_SESSION['status'])&& $_SESSION['unm']=="admin"))
	{
		include("includes/sidebar.php");
		
	}
	else
	{
		include("includes/sidebar-admin.php");
		}

?>
        

		<div class="col-md-8">
			<div class="row">


				<div class=" col-md-8 my-profile entry">
				
				<form action='process_addsubcategory.php' method='POST'>
							<h3 style="color:darkgreen">Add Suburb </h3>
							<br><br>
							Region 
									<br>
										<select  name="parent">
											<?php
											
												require('includes/config.php');
					
													$query="select * from region ";
					
													$res=mysqli_query($conn,$query);
													
													while($row=mysqli_fetch_assoc($res))
													{
														echo "<option value='".$row['cat_id']."'>".$row['cat_nm'];
														//PASS ID NOT NAME
													}
					
													//mysqli_close($link);
											?>
										</select>
									
									<br>
							Suburb:
									<br><br>
										<input type='text' name= 'subcat' size='25'>
										
										<input type='submit'  value='  ADD  '>
									
									<br><br>	
				</form>
				<hr>
				<form action='process_delcategory.php' method='POST'>
							
						
				<form action='process_delsubcategory.php' method='POST'>
						
						<b style="color:darkgreen">Delete Suburb </b>						
							<br>
								<select  name="subcatnm">
									<?php
									
										
			
											$query="select * from region ";
			
											$res=mysqli_query($conn,$query);
											
											while($row=mysqli_fetch_assoc($res))
											{
												echo "<option>".$row['cat_nm'];
												$qq = "select * from suburb where parent_id=".$row['cat_id'];
												
												$ress = mysqli_query($conn,$qq) or die("wrong delete subcat query..");
												while($roww = mysqli_fetch_assoc($ress))
												{
													echo "<option value='".$roww['subcat_id']."'> <b>-</b> ".$roww['subcat_nm'];
												}
												
											}
			
											//mysqli_close($link);
									?>
								</select>
						
							
							<input type='submit' value=' DELETE '>
				</form>	
			</div>

				<div class="col-md-4">
					<!-- Avatar -->
					<div class="edit-profile-photo">
						<img src="images/agent-02.jpg" alt="">
						<div class="change-photo-btn">
							<div class="photoUpload">
							    <span><i class="fa fa-upload"></i> Upload Photo</span>
							    <input type="file" class="upload" />
							</div>
						</div>
					</div>

				</div>


			</div>
		</div>

	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>


<!-- Style Switcher
================================================== -->
<script src="scripts/switcher.js"></script>


<!-- Style Switcher / End -->


</div>
<!-- Wrapper / End -->


</body>
</html>