<?php
session_start();

?>
<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">


	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->


<?php
require('includes/config.php');

$query = "SELECT * FROM book inner join subcat on b_subcat = subcat_id INNER JOIN category ON cat_id = parent_id  where b_id=".$_GET['id'];
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_assoc($result);



//var_dump($row);
?>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="property-titlebar margin-bottom-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<a href="#" class="back-to-listings"></a>
				<div class="property-title">
					<h2><?php echo $row['b_nm'];?> <span class="property-badge"><?php echo $row['subcat_nm'];?></span></h2>
					<span>
						<a href="#location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							<?php  echo $row['b_edition'].', '.$row['b_publisher'];?>
						</a>
					</span>
				</div>

				<div class="property-pricing">
					<div>$<?php echo $row["b_price"]; if($row['subcat_id'] ==2 ||$row['subcat_id'] ==4||$row['subcat_id'] ==7)
						echo ' monthly';
						?></div>
					<div class="sub-price">$<?php echo number_format((float)$row['b_price']/$row['b_area'], 2, '.', '');?> / sq m</div>
				</div>


			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row margin-bottom-50">
		<div class="col-md-12">
		
			<!-- Slider Container -->
			<div class="property-slider-container">

				<!-- Agent Widget -->
				<div class="agent-widget">
					<div class="agent-title">
						<div class="agent-photo"><img src="images/agent-avatar.jpg" alt="" /></div>
						<div class="agent-details">
							<h4><a href="#"><?php echo $row['b_user'];?></a></h4>
							<span><i class="sl sl-icon-call-in"></i><?php echo $row['b_contact'];?></span>
						</div>
						<div class="clearfix"></div>
					</div>
                    <div id="contact-message"></div> 
<form method="post" action="contact.php" name="contactform" id="contactform" autocomplete="on">
<input name="subject" type="text" id="subject" placeholder="Your Name" required />
					<input name="email" type="email" id="email" placeholder="Email Address" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required />
					<input type="text" name="name" id="name" placeholder="Your Phone">
					<textarea name="comments" id="comments">I'm interested in this <?php echo $row['b_nm'];?>  property and I'd like to know more details.</textarea>
					<button class="button fullwidth margin-top-5">Send Message</button>
                    </form>
				</div>
				<!-- Agent Widget / End -->

				<!-- Slider -->
				<div class="property-slider no-arrows">
					<a href="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" class="item mfp-gallery"></a>
					<?php if (!empty($row['b_img2'])){
					?>
					<a href="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-3-'.$row['b_img3'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-3-'.$row['b_img3'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-4-'.$row['b_img4'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-4-'.$row['b_img4'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-5-'.$row['b_img5'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-5-'.$row['b_img5'];?>" class="item mfp-gallery"></a>
                    
                    <a href="upload_image/<?php echo $row['b_id'].'-6-'.$row['b_img6'];?>" data-background-image="upload_image/<?php echo $row['b_id'].'-6-'.$row['b_img6'];?>" class="item mfp-gallery"></a>
                    
					<?php }?>
                    
                    
					<!--<a href="images/single-property-03.jpg" data-background-image="images/single-property-03.jpg" class="item mfp-gallery"></a>
					<a href="images/single-property-04.jpg" data-background-image="images/single-property-04.jpg" class="item mfp-gallery"></a>
					<a href="images/single-property-05.jpg" data-background-image="images/single-property-05.jpg" class="item mfp-gallery"></a>
					<a href="images/single-property-06.jpg" data-background-image="images/single-property-06.jpg" class="item mfp-gallery"></a>-->
				</div>
				<!-- Slider / End -->

			</div>
			<!-- Slider Container / End -->

			<!-- Slider Thumbs -->
			<div class="property-slider-nav">
				<div class="item"><img src="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" alt=""></div>
				<?php if (!empty($row['b_img2'])){
				?>
				<div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-3-'.$row['b_img3'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-4-'.$row['b_img4'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-5-'.$row['b_img5'];?>" alt=""></div>
                
                <div  class="item"><img src="upload_image/<?php echo $row['b_id'].'-6-'.$row['b_img6'];?>" alt=""></div>
                
				<?php }?>
                
			<!--	<div class="item"><img src="images/single-property-02.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-03.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-04.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-05.jpg" alt=""></div>
				<div class="item"><img src="images/single-property-06.jpg" alt=""></div>-->
			</div>

		</div>
	</div>
</div>


<div class="container">
	<div class="row">

		<!-- Property Description -->
		<div class="col-lg-8 col-md-7">
			<div class="property-description">

				<!-- Main Features -->
				<ul class="property-main-features">
					<li>Area <span><?php echo $row['b_area'];?> sq ft</span></li>
					
					<li>Bedrooms <span><?php echo $row['b_isbn'];?></span></li>
					<li>Bathrooms <span><?php echo $row['b_page'];?></span></li>
				</ul>


				<!-- Description -->
				<h3 class="desc-headline">Description</h3>
				<div>
					<?php echo $row['b_desc'];?>
					
				</div>

				<!-- Details -->
				


				<!-- Features -->
				<h3 class="desc-headline">Features</h3>
				<ul class="property-features checkboxes margin-top-0">
					<?php if ($row['b_ac']==1){?>
					<li>Air Conditioning</li>

					<?php } if ($row['b_sp']==1){?>
					<li>Swimming Pool</li>

					<?php } if ($row['b_ch']==1){?>
					<li>Central Heating</li>

					<?php } if ($row['b_lr']==1){?>
					<li>Laundry Room</li>

					<?php } if ($row['b_gym']==1){?>
					<li>Gym</li>

					<?php } if ($row['b_al']==1){?>
					<li>Alarm</li>

					<?php } if ($row['b_wc']==1){?>
					<li>Window Covering</li>

					
					<?php }?>
				</ul>


				<!-- Location -->
				

			

			</div>
		</div>
		<!-- Property Description / End -->


		<!-- Sidebar -->
		<div class="col-lg-4 col-md-5">
			<div class="sidebar sticky right">

				<!-- Widget -->
				<div class="widget margin-bottom-35">
					<button class="widget-button"><i class="sl sl-icon-printer"></i> Print</button>
					<button class="widget-button save" data-save-title="Save" data-saved-title="Saved"><span class="like-icon"></span></button>
				</div>
				<!-- Widget / End -->


				<!-- Widget -->
				<div class="widget">
					<p><!-- Currency Converter Script - EXCHANGERATEWIDGET.COM -->
<div style="width:270px;border:1px solid #55A516;"><div style="text-align:center;background-color:#55A516;width:270px;height:400px;font-size:13px;font-weight:bold;height:25px;padding-top:2px;"><a href="#" style="color:#FFFFFF;text-decoration:none;" rel="nofollow">Currency Converter</a></div><script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=en&f=USD&t=EUR&a=1&d=F0F0F0&n=FFFFFF&o=000000&v=1"></script></div>
<!-- End of Currency Converter Script --></p>
						
						<!-- Item -->
						<div class="item">
							
						</div>
						<!-- Item / End -->

						<!-- Item --
						<div class="item">
							<div class="listing-item compact">

								<a href="#" class="listing-img-container">

									<div class="listing-badges">
										<span class="featured">Featured</span>
										<span>For Sale</span>
									</div>

									<div class="listing-img-content">
										<span class="listing-compact-title">Selway Apartments <i>$245,000</i></span>

										<ul class="listing-hidden-content">
											<li>Area <span>530 sq ft</span></li>
											<li>Rooms <span>3</span></li>
											<li>Beds <span>1</span></li>
											<li>Baths <span>1</span></li>
										</ul>
									</div>

									<img src="images/listing-02.jpg" alt="">
								</a>

							</div>
						</div>
						<!-- Item / End -->

						<!-- Item --
						<div class="item">
							<div class="listing-item compact">

								<a href="#" class="listing-img-container">

									<div class="listing-badges">
										<span class="featured">Featured</span>
										<span>For Sale</span>
									</div>

									<div class="listing-img-content">
										<span class="listing-compact-title">Oak Tree Villas <i>$325,000</i></span>

										<ul class="listing-hidden-content">
											<li>Area <span>530 sq ft</span></li>
											<li>Rooms <span>3</span></li>
											<li>Beds <span>1</span></li>
											<li>Baths <span>1</span></li>
										</ul>
									</div>

									<img src="images/listing-03.jpg" alt="">
								</a>

							</div>
						</div>
						<!-- Item / End -->
					</div>

				</div>
				<!-- Widget / End -->

			

			</div>
		</div>
		<!-- Sidebar / End -->

	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>

<!-- Maps -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript" src="scripts/infobox.min.js"></script>
<script type="text/javascript" src="scripts/markerclusterer.js"></script>
<script type="text/javascript" src="scripts/maps.js"></script>


<!-- Style Switcher
================================================== -->



<!-- Style Switcher / End -->


</div>
<!-- Wrapper / End -->


</body>
</html>