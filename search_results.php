<?php
session_start();

?>
<!DOCTYPE html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">
<link rel="stylesheet" href="css/nivo-slider.css" media="screen">
 <link rel="stylesheet" href="css/animations.css" media="screen">

	<!-- Scripts
================================================== -->
	<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
	<script type="text/javascript" src="scripts/chosen.min.js"></script>
	<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
	<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
	<script type="text/javascript" src="scripts/rangeSlider.js"></script>
	<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
	<script type="text/javascript" src="scripts/slick.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
	<script type="text/javascript" src="scripts/tooltips.min.js"></script>
	<script type="text/javascript" src="scripts/masonry.min.js"></script>
	<script type="text/javascript" src="scripts/custom.js"></script>
	<script src="js/jquery.nivo.slider.pack.js"></script>
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.parallax.js"></script>
	<script src="js/jquery.wait.js"></script>
	<script src="js/modernizr-2.6.2.min.js"></script>
	<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
	<script src="js/jquery.nivo.slider.pack.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/superfish.js"></script>
	<script src="js/tweetMachine.js"></script>
	<script src="js/tytabs.js"></script>
	<script src="js/jquery.gmap.min.js"></script>
	<script src="js/circularnav.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jflickrfeed.js"></script>
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/spectrum.js"></script>
	<script src="js/switcher.js"></script>
	<script src="js/custom.js"></script>

	<!-- Style Switcher
    ================================================== -->
	<script src="scripts/switcher.js"></script>


	<!-- Style Switcher / End -->

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/colors/main.css" id="colors">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="jquery.js"></script>
	<script type='text/javascript' src='jquery.autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />

	<script type="text/javascript">
		$().ready(function() {
			$("#search").autocomplete("get_course_list.php", {

				width: 260,
				matchContains: true,
				mustMatch: true,
				minChars: 0,
				//multiple: true,
				highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});

		});
	</script>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">

	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->


</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Search
================================================== -->
<section class="search margin-bottom-50">

</section>



<!-- Content
================================================== -->
<div class="container">
	<div class="row fullwidth-layout">

		<div class="col-md-12">

			<form autocomplete="off" action="search_results.php" method="post">
        <div class="main-search-input margin-bottom-35">
				<input type="text"  id="search" name="course" class="ico-01" placeholder="Enter address e.g. suburb and city " value=""/>
				<button class="button">Search</button>
			</div>
				</form>

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-15">

				<div class="col-md-6">
					<!-- Sort by -->
					<div class="sort-by">
						<label>Sort by:</label>

						<div class="sort-by-select">
							<select data-placeholder="Default order" class="chosen-select-no-single" >
								<option>Default Order</option>	
								<option>Price Low to High</option>
								<option>Price High to Low</option>
								<option>Newest Properties</option>
								<option>Oldest Properties</option>
							</select>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						<a href="#" class="grid-three"><i class="fa fa-th"></i></a>
						<a href="#" class="grid"><i class="fa fa-th-large"></i></a>
						<a href="#" class="list"><i class="fa fa-th-list"></i></a>
					</div>
				</div>
			</div>

			
			<!-- Listings -->
			<div class="listings-container grid-layout-three">
				<?php
				require('includes/config.php');

				$sqlfilter="1";

				if (isset($_POST['course']) && $_POST['course'] != "" )
					$sqlfilter .=" AND b_edition = '".$_POST['course']."'";


				if (isset($_POST['type']) && $_POST['type'] != "")
					$sqlfilter .=" AND cat_nm = '".$_POST['type']."'";

				if (isset($_POST['status']) && $_POST['status'] != "")
					$sqlfilter .=" AND subcat_nm = '".$_POST['status']."'";

				if (isset($_POST['price']) && $_POST['price'] != "")
					$sqlfilter .=" AND b_price  BETWEEN 0 AND ".$_POST['price'];

				$query="select * from book inner join subcat on b_subcat = subcat_id INNER JOIN category ON cat_id = parent_id where ".$sqlfilter."  ORDER BY b_id DESC ";
//echo $query; exit;
				$res=mysqli_query($conn,$query);

				while($row=mysqli_fetch_assoc($res)) {
					//echo "<option>".$row['cat_nm'];
					//var_dump($row);
					?>
					<!-- Listing Item -->
					<div class="listing-item">

						<a href="single-property-page.php?id=<?php echo $row['b_id'];?>" class="listing-img-container">

							<div class="listing-badges">
								
								<span class="featured">Featured</span>
								
								<span><?php echo $row['subcat_nm'];?></span>
							</div>

							<div class="listing-img-content">
								<span class="listing-price">$<?php echo $row['b_price']; if($row['subcat_id'] ==2 ||$row['subcat_id'] ==4||$row['subcat_id'] ==7)
									echo ' monthly';
									?> <i>$<?php echo number_format((float)$row['b_price']/$row['b_area'], 2, '.', '');?> / sq ft</i></span>
								<span class="like-icon tooltip"></span>
							</div>

							<div class="listing-carousel">
								<div><img src="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" alt=""></div>
								<?php
								if (!empty($row['b_img2'])){
								?>
								<div><img src="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" alt=""></div>
								<?php }?>
							<!--	<div><img src="images/listing-01c.jpg" alt=""></div>-->
							</div>
						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="single-property-page.php?id=<?php echo $row['b_id'];?>"><?php echo $row['b_nm'];?></a></h4>
								<a href="https://maps.google.com/maps?q=<?php echo $row['b_address'].',+'.$row['b_edition'].',+'. $row['b_publisher'];?>,+zimbabwe"
								   class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									 <?php  echo $row['b_address'].', '.$row['b_edition'].', '.$row['b_publisher'];?>
								</a>

								<a href="single-property-page.php?id=<?php echo $row['b_id'];?>" class="details button border">Details</a>
							</div>

							<ul class="listing-details">
								<li><?php echo $row['b_area'];?> sq ft</li>
								<li><?php echo $row['b_isbn'];?> Bedroom</li>
								<li><?php echo $row['b_rooms'];?> Rooms</li>
								<li><?php echo $row['b_page'];?> Bathroom</li>
							</ul>

							<div class="listing-footer">
								<a href="#"><i class="fa fa-user"></i><?php echo $row['b_user']?></a>
								<span><i class="fa fa-calendar-o"></i> 	<?php
									$zuva=explode(' ',$row['b_date']);
									list($yy,$mm,$dd)=explode('-',$zuva[0]);

									$start=date($yy.'-'.$mm.'-'.$dd);
									$end =date("Y-m-d");


									$tZone = new DateTimeZone('GMT');
									$dt1 = new DateTime($start, $tZone);
									$dt2 = new DateTime($end, $tZone);
									$ts1 = $dt1->format('Y-m-d');
									$ts2 = $dt2->format('Y-m-d');
									$diff = abs(strtotime($ts1)-strtotime($ts2));
									$diff/= 3600*24;

									echo $diff." days ago";
									?></span>
							</div>

						</div>

					</div>
					<?php
				}
				?>

			</div>
			<!-- Listings Container / End -->

			<div class="clearfix"></div>
			<!-- Pagination -->
			<div class="pagination-container margin-top-20">
				<nav class="pagination">
					<ul>
						<li><a href="#" class="current-page">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li class="blank">...</li>
						<li><a href="#">22</a></li>
					</ul>
				</nav>

				<nav class="pagination-next-prev">
					<ul>
						<li><a href="#" class="prev">Previous</a></li>
						<li><a href="#" class="next">Next</a></li>
					</ul>
				</nav>
			</div>
			<!-- Pagination / End -->

		</div>

	</div>
</div>
			
			<!-- Pagination -->
			
			<!-- Pagination / End -->

	


		<!-- Sidebar
		================================================== -->
		
		</div>
		<!-- Sidebar / End -->
	</div>
	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>



<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>





</div>
<!-- Wrapper / End -->


</body>
</html>