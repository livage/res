<?php
require('includes/config.php');
 session_start();


	//$cat=$_GET['cat_nm'];
	
	$q = "select * from subcat where parent_id = ".$_GET['cat'];
	$res = mysqli_query($conn,$q) or die("Can't Execute Query..");
	
	$row1 = mysqli_fetch_assoc($res);
	
	if($_GET['catnm']==$row1['subcat_nm'])
	{
		header("location:booklist.php?subcatid=".$row1['subcat_id']."&subcatnm=".$row1["subcat_nm"]);
		
	}
?>
<!DOCTYPE html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Scripts
================================================== -->
	<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
	<script type="text/javascript" src="scripts/chosen.min.js"></script>
	<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
	<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
	<script type="text/javascript" src="scripts/rangeSlider.js"></script>
	<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
	<script type="text/javascript" src="scripts/slick.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
	<script type="text/javascript" src="scripts/tooltips.min.js"></script>
	<script type="text/javascript" src="scripts/masonry.min.js"></script>
	<script type="text/javascript" src="scripts/custom.js"></script>




	<!-- Style Switcher
    ================================================== -->
	<script src="scripts/switcher.js"></script>
	<!-- Style Switcher / End -->

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">
<link rel="stylesheet" href="css/nivo-slider.css" media="screen">
 <link rel="stylesheet" href="css/animations.css" media="screen">
<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/colors/main.css" id="colors">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="jquery.js"></script>
	<script type='text/javascript' src='jquery.autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />

	<script type="text/javascript">
		$().ready(function() {
			$("#search").autocomplete("get_course_list.php", {

				width: 260,
				matchContains: true,
				mustMatch: true,
				minChars: 0,
				//multiple: true,
				highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});

		});
	</script>
</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">

	<!-- Header -->
	<?php
							include("includes/header.php");
						?>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Search
================================================== -->
<section class="search margin-bottom-50">

</section>



<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
    
<h3 class="margin-top-0 margin-bottom-35"><?php echo $_GET['catnm'];?></h3>

<div class="slider-wrapper theme-default">
               <div id="nivoslider" class="nivoSlider">
                  <a href="#"><img src="images/pages.jpg" ></a>
                   <a href="#"><img src="images/pages2.jpg" ></a>
                  <a href="#"><img src="images/pages3.jpg"></a>
               </div>
            </div><br><br>
		<div class="col-md-8">
        
<form autocomplete="off" action="search_results.php" method="post">
        <div class="main-search-input margin-bottom-35">
				<input type="text"  id="search" name="course" class="ico-01" placeholder="Enter address e.g. suburb and city " value=""/>
				<button class="button">Search</button>
			</div>
				</form>
			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-15">

				<div class="col-md-6">
					<!-- Sort by -->
					
				</div>

				<div class="col-md-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						<a href="#" class="list"><i class="fa fa-th-list"></i></a>
						<a href="#" class="grid"><i class="fa fa-th-large"></i></a>
					</div>
				</div>
			</div>

			
			<!-- Listings -->
			<div class="listings-container grid-layout">

				<?php
				require('includes/config.php');

				$sqlfilter="1";

				if (isset($_POST['course']) && $_POST['course'] != "" )
					$sqlfilter .=" AND b_publisher = '".$_POST['course']."'";


				//if (isset($_POST['type']) && $_POST['type'] != "")
					$sqlfilter .=" AND cat_nm = '".$_GET['catnm']."'";

				if (isset($_POST['status']) && $_POST['status'] != "")
					$sqlfilter .=" AND subcat_nm = '".$_POST['status']."'";

				if (isset($_POST['price']) && $_POST['price'] != "")
					$sqlfilter .=" AND b_price  BETWEEN 0 AND ".$_POST['price'];


				$query="select * from book inner join subcat on b_subcat = subcat_id INNER JOIN category ON cat_id = parent_id where ".$sqlfilter."  ORDER BY b_id DESC ";

				$res=mysqli_query($conn,$query);

				while($row=mysqli_fetch_assoc($res)) {
					//echo "<option>".$row['cat_nm'];
					//var_dump($row);
					?>


					<!-- Listing Item -->
					<div class="listing-item">

						<a href="single-property-page.php?id=<?php echo $row['b_id'];?>" class="listing-img-container">

							<div class="listing-badges">
								<?php if ($row['b_feature']==1){?>
									<span class="featured">Featured</span>
								<?php }?>
								<span><?php echo $row['subcat_nm'];?></span>
							</div>

							<div class="listing-img-content">
								<span class="listing-price">$<?php echo $row['b_price']; if($row['subcat_id'] ==2 ||$row['subcat_id'] ==4||$row['subcat_id'] ==7)
										echo ' monthly';
									?> <i>$<?php echo number_format((float)$row['b_price']/$row['b_area'], 2, '.', '');?> / sq ft</i></span>
								<span class="like-icon tooltip"></span>
							</div>

							<div class="listing-carousel">
								<div><img src="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" alt=""></div>
								<?php
								if (!empty($row['b_img2'])){
									?>
									<div><img src="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" alt=""></div>
								<?php }?>
							</div>
						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="#"><?php echo $row['b_nm'];?></a></h4>
								<a href="https://maps.google.com/maps?q=<?php echo $row['b_address'].',+'.$row['b_edition'].',+'. $row['b_publisher'];?>,+zimbabwe"
								   class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									<?php  echo $row['b_address'].', '.$row['b_edition'].', '.$row['b_publisher'];?>
								</a>

								<a href="single-property-page.php?id=<?php echo $row['b_id'];?>" class="details button border">Details</a>
							</div>

							<ul class="listing-details">
								<li><?php echo $row['b_area'];?> sq ft</li>
								<li><?php echo $row['b_isbn'];?> Bedroom</li>
								<li><?php echo $row['b_rooms'];?> Rooms</li>
								<li><?php echo $row['b_page'];?> Bathroom</li>
							</ul>

							<div class="listing-footer">
								<a href="#"><i class="fa fa-user"></i> <?php echo $row['b_user']?></a>
								<span><i class="fa fa-calendar-o"></i> <?php
									$zuva=explode(' ',$row['b_date']);
									list($yy,$mm,$dd)=explode('-',$zuva[0]);

									$start=date($yy.'-'.$mm.'-'.$dd);
									$end =date("Y-m-d");


									$tZone = new DateTimeZone('GMT');
									$dt1 = new DateTime($start, $tZone);
									$dt2 = new DateTime($end, $tZone);
									$ts1 = $dt1->format('Y-m-d');
									$ts2 = $dt2->format('Y-m-d');
									$diff = abs(strtotime($ts1)-strtotime($ts2));
									$diff/= 3600*24;

									echo $diff." days ago";
									?></span>
							</div>

						</div>

					</div>

					<?php
				}
					?>
			</div>
			<!-- Listings Container / End -->

			
			<!-- Pagination -->
			
			<!-- Pagination / End -->

		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-md-4">
			<div class="sidebar sticky right">

				<!-- Widget -->
				<div class="widget margin-bottom-40">

 <p><!-- Currency Converter Script - EXCHANGERATEWIDGET.COM -->
<div style="width:270px;border:1px solid #55A516;"><div style="text-align:center;background-color:#55A516;width:270px;height:400px;font-size:13px;font-weight:bold;height:25px;padding-top:2px;"><a href="#" style="color:#FFFFFF;text-decoration:none;" rel="nofollow">Currency Converter</a></div><script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=en&f=USD&t=EUR&a=1&d=F0F0F0&n=FFFFFF&o=000000&v=1"></script></div>
<!-- End of Currency Converter Script --></p>

<br><br>

                <div style="width:270px; text-align:center;"><p style="background-color:#F4F7F9;"><a href="http://www.mortgagecalculator.org/" target="_blank"><img src="http://www.mortgagecalculator.org/free-tools/calculator/mortgage-calculator-logo.png" alt="Property Exchange"></a><br><iframe src="http://www.mortgagecalculator.org/free-tools/calculator/caller.html" frameborder="0" width="270px" height="400" scrolling="no"></iframe><br><a href="http://www.mortgagecalculator.org/free-tools/"><font color="#000000"> </font></a></p></div>
									
                                    

					
                   
					</div>

				</div>
				<!-- Widget / End -->

			</div>
		</div>
		<!-- Sidebar / End -->
	</div>
	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<?php
							include("includes/footer.php");
						?>



<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<!--<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>-->
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
 <script src="js/jquery.nivo.slider.pack.js"></script>
  <script src="js/imagesloaded.pkgd.min.js"></script>
  <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.js"></script>
      <script src="js/jquery.parallax.js"></script>
      <script src="js/jquery.wait.js"></script>
      <script src="js/modernizr-2.6.2.min.js"></script>
      <script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
      <script src="js/jquery.nivo.slider.pack.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/superfish.js"></script>
      <script src="js/tweetMachine.js"></script>
      <script src="js/tytabs.js"></script>
      <script src="js/jquery.gmap.min.js"></script>
      <script src="js/circularnav.js"></script>
      <script src="js/jquery.sticky.js"></script>
      <script src="js/jflickrfeed.js"></script>
      <script src="js/imagesloaded.pkgd.min.js"></script>
      <script src="js/waypoints.min.js"></script>
      <script src="js/spectrum.js"></script>
      <script src="js/switcher.js"></script>
      <script src="js/custom.js"></script>


<!-- Style Switcher
================================================== -->
<script src="scripts/switcher.js"></script>
<!--<script type="text/javascript" src="js/jquery-ui.js"></script>-->
<script src="js/jquery-migrate-1.2.1.min.js"></script>

<script type='text/javascript' src='jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />
<!-- Style Switcher / End -->


</div>
<!-- Wrapper / End -->


</body>
</html>