<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo" class="margin-top-10">
					<a href="index.php"><img src="images/official logo.jpg" alt=""></a>

					<!-- Logo for Sticky Header -->
					<a href="index.php" class="sticky-logo"><img src="#" alt=""></a>
				</div>
				
			</div>
			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->
			<div class="right-side">
				<!-- Header Widget -->
				<ul class="header-widget">
					<li>
						<i class="sl sl-icon-call-in"></i>
						<div class="widget-content">
							<span class="title">Enquiries</span>
							<span class="data">+263 4 304496  </span>
						</div>
					</li>

					<li>
						<i class="sl sl-icon-location"></i>
						<div class="widget-content">
							<span class="title">Find us at:</span>
							<span class="data">134 King George Ave, Harare</span>
						</div>
					</li>

					<li class="with-btn"><?php
						require('includes/config.php');
							if(isset($_SESSION['status']))
							{
								echo '<h4>Hi :  '.$_SESSION['unm'].'</h4>';
							}
							else
							{
								echo '<a href="login-register.php" class="button border">Login / Register</a>';
							}
								?></li>
				</ul>
				<!-- Header Widget / End -->
			</div>
			<!-- Right Side Content / End -->

		</div>


		<!-- Mobile Navigation -->
		<div class="menu-responsive">
			<i class="fa fa-reorder menu-trigger"></i>
		</div>


		<!-- Main Navigation -->
		<nav id="navigation" class="style-2">
			<div class="container">
					<ul id="responsive">

						<li><?php
						require('includes/config.php');
							if(isset($_SESSION['status']))
							{
								echo '<a href="index.php">Home</a>';
							}
							else
							{
								echo '<a href="index.php">Home</a>';
							}
								?>
							
						</li>

						<li><a href="about_us.php">About Us</a>
							<ul>
										<li><a href="services.php">Our Services</a></li>
										
									</ul>
								
						</li>

						<li><a href="#">Available Properties</a>
							<ul>
										<?php
										
			
										$query="select * from category ";
			
										$res=mysqli_query($conn,$query);
											
										while($row=mysqli_fetch_assoc($res))
											{
												echo '<li><a href="subcat.php?cat='.$row['cat_id'].'&catnm='.$row["cat_nm"].'">'.$row["cat_nm"].'</a></li>';
												//pass catid not catnm
											}
			
											mysqli_close($conn);
								?>
									</ul>
						</li>

						<li><a href="developments.php">Developments</a>
							
						</li>
<li><a href="showdays.php">Showdays</a>
							
						</li>
                        <li><a href="contact1.php">Contact Us</a>
							
						</li>
                        <?php 
					if(isset($_SESSION['status']))
					{
						
						echo '<li><a href="submit-property.php">Submit Property</a></li>';
					}
					
			?>
<?php 
					if(isset($_SESSION['status']))
					{
						
						echo '<li><a href="logout.php">Logout</a></li>';
					}
					
			?>
         

					</ul>
			</div>
		</nav>
		<div class="clearfix"></div>
		<!-- Main Navigation / End -->
	</div>