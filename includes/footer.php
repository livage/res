<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo" src="images/official logo.jpg" alt="">
				<br><br>
				<p>Our mission is to be the leaders in the property market through excellent service and provision of a diverse range of realty products and services to the satisfaction of all our stakeholders</p>
		  </div>

			<div class="col-md-4 col-sm-6 ">
				<h4>Helpful Links</h4>
				<ul class="footer-links">
					<li><a href="login-register.php">Login</a></li>
					<li><a href="login-register2.php">Sign Up</a></li>
					<li><a href="login-register.php">My Account</a></li>
					
					
				</ul>

				<ul class="footer-links">
					<li><a href="faqs.php">FAQ</a></li>
					
					<li><a href="contact1.php">Contact</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
					<span>134 King George Ave, Avondale, Harare</span> <br>
					Phone: <span>+263 4 304496 </span><br>
					E-Mail:<span> <a href="mailto:sale@propertyexchange.co.zw">sale@propertyexchange.co.zw</a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="https://www.facebook.com/www.propertyexchange.co.zw/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/propertyexchnge" target="_blank"><i class="icon-twitter"></i></a></li>
                    <li><a class="instagram" href="https://www.instagram.com/propertyexchangezimbabwe/?hl=en" target="_blank"><i class="icon-instagram"></i></a></li>
                    
                     <li><a class="youtube" href="#" target="_blank"><i class="icon-youtube"></i></a></li>
                   
					
				</ul>

			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© 2017 Property Exchange. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div>