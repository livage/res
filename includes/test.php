
<!DOCTYPE html>
<head>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Basic Page Needs
================================================== -->
<title>Property Exchange</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/main.css" id="colors">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="jquery.js"></script>
<script type='text/javascript' src='jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />

<script type="text/javascript">
    $().ready(function() {
        $("#course").autocomplete("get_course_list.php", {
		width: 260,
		matchContains: true,
		//mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
});
</script>
</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="header-style-2">

	<!-- Header -->
	<div id="header">
		<div class="container">

			<!-- Left Side Content -->
			<div class="left-side">

				<!-- Logo -->
				<div id="logo" class="margin-top-10">
					<a href="index.php"><img src="images/official logo.jpg" alt=""></a>

					<!-- Logo for Sticky Header -->
					<a href="index.php" class="sticky-logo"><img src="#" alt=""></a>
				</div>

			</div>
			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->
			<div class="right-side">
				<!-- Header Widget -->
				<ul class="header-widget">
					<li>
						<i class="sl sl-icon-call-in"></i>
						<div class="widget-content">
							<span class="title">Enquiries</span>
							<span class="data">+263 4 304496  </span>
						</div>
					</li>

					<li>
						<i class="sl sl-icon-location"></i>
						<div class="widget-content">
							<span class="title">Find us at:</span>
							<span class="data">134 King George Ave, Harare</span>
						</div>
					</li>

					<li class="with-btn"><a href="login-register.php" class="button border">Login / Register</a></li>
				</ul>
				<!-- Header Widget / End -->
			</div>
			<!-- Right Side Content / End -->

		</div>


		<!-- Mobile Navigation -->
		<div class="menu-responsive">
			<i class="fa fa-reorder menu-trigger"></i>
		</div>


		<!-- Main Navigation -->
		<nav id="navigation" class="style-2">
			<div class="container">
					<ul id="responsive">

						<li><a href="index.php">Home</a>
						</li>

						<li><a href="about_us.php">About Us</a>
							<ul>
										<li><a href="services.php">Our Services</a></li>

									</ul>

						</li>

						<li><a href="#">Available Properties</a>
							<ul>
										<li><a href="subcat.php?cat=1&catnm=Houses">Houses</a></li><li><a href="subcat.php?cat=2&catnm=Stands">Stands</a></li><li><a href="subcat.php?cat=3&catnm=Commercial">Commercial</a></li><li><a href="subcat.php?cat=4&catnm=Plots">Plots</a></li>									</ul>
						</li>

						<li><a href="developments.php">Developments</a>

						</li>
<li><a href="showdays.php">Showdays</a>

						</li>
                        <li><a href="contact1.php">Contact Us</a>

						</li>


					</ul>
			</div>
		</nav>
		<div class="clearfix"></div>
		<!-- Main Navigation / End -->
	</div>	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->


<!-- Banner
================================================== -->
<div class="parallax" data-background="images/home-parallax.jpg" data-color="#36383e" data-color-opacity="0.5" data-img-width="2500" data-img-height="1600">

	<div class="container">
		<div class="row">

            <div class="col-md-12">

				<div class="search-container">

					<!-- Form -->
					<h2>Find a New Home</h2>

					<!-- Row With Forms -->
				  <div class="row with-forms">

						<!-- Property Type -->
						<div class="col-md-3">
                         <form autocomplete="off" action="search_results.php" method="post">
							<select data-placeholder="Any Type" class="chosen-select-no-single"  >
								<option>Houses</option>
								<option>Flats</option>
								<option>Commercial</option>
								<option>Stands</option>
								<option>Plots</option>
							</select>
						</div>

						<!-- Status -->
						<div class="col-md-3">
							<select data-placeholder="Any Status" class="chosen-select-no-single"  >
								<option>For Sale</option>
								<option>For Rent</option>
							</select>
						</div>

						<!-- Main Search Input -->
						<div class="col-md-6">
							<div class="main-search-input">

								<input name="course" id="course" type="text" placeholder="Enter address e.g. province, city or suburb" value=""/>
								<button class="button"><i class="fa fa-search"></i></button>


							</div>
						</div>

					</div>
					<!-- Row With Forms / End -->

					<!-- Browse Jobs -->

                    <div class="range-slider">
						<label>Price Range</label>
						<div id="price-range" data-min="0" data-max="3000000" data-unit="$"></div>
						<div class="clearfix"></div>
					</div>
					<div class="adv-search-btn range-slider">
					 <a href="listings-list-full-width.html">Advanced Search</a>
                        </div>

 </form>
				</div>

			</div>
		</div>
	</div>

</div>


<!-- Content
================================================== -->


<!-- Featured -->
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline margin-bottom-25 margin-top-65">Newly Added</h3>
		</div>

		<!-- Carousel -->
		<div class="col-md-12">
			<div class="carousel">
				<!-- Listing Item -->
					<div class="carousel-item">
					<div class="listing-item">

						<a href="#" class="listing-img-container">

							<div class="listing-badges">
								<span class="featured">Featured</span>
								<span>For Sale</span>
							</div>

							<div class="listing-img-content">
								<span class="listing-price">$8890</i></span>
								<span class="like-icon tooltip"></span>
							</div>

							<div class="listing-carousel">
								<div><img src="upload_image/79-1-007.JPG" alt="" width="520px" height="397px"></div>
								<!--<div><img src="upload_image/79-2-011.JPG" alt="" width="520" height="397"></div>
								<div><img src="upload_image/007.JPG" alt="" width="520" height="397"></div>-->
							</div>

						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="#">avondale harare</a></h4>

							</div>

							<ul class="listing-features">
								<li>Area <span>530 sq m</span></li>
								<li><i class="fa fa-bed" aria-hidden="true"></i>
 <span>1 </span></li>
								<li><i class="fa fa-bathtub"></i>
<span>1</span></li>
							</ul>

							<div class="listing-footer">
								<a href="#"><i class="fa fa-user"></i> admin</a>
								<span><i class="fa fa-calendar-o"></i>
0 days ago								 </span>
							</div>

						</div>

					</div>
				</div>

				<!-- Listing Item / End -->
              				<!-- Listing Item -->
					<div class="carousel-item">
					<div class="listing-item">

						<a href="#" class="listing-img-container">

							<div class="listing-badges">
								<span class="featured">Featured</span>
								<span>For Sale</span>
							</div>

							<div class="listing-img-content">
								<span class="listing-price">$12345</i></span>
								<span class="like-icon tooltip"></span>
							</div>

							<div class="listing-carousel">
								<div><img src="upload_image/78-1-006.JPG" alt="" width="520px" height="397px"></div>
								<!--<div><img src="upload_image/78-2-012.JPG" alt="" width="520" height="397"></div>
								<div><img src="upload_image/006.JPG" alt="" width="520" height="397"></div>-->
							</div>

						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="#">avondale harare</a></h4>

							</div>

							<ul class="listing-features">
								<li>Area <span>530 sq m</span></li>
								<li><i class="fa fa-bed" aria-hidden="true"></i>
 <span>4 </span></li>
								<li><i class="fa fa-bathtub"></i>
<span>2</span></li>
							</ul>

							<div class="listing-footer">
								<a href="#"><i class="fa fa-user"></i> admin</a>
								<span><i class="fa fa-calendar-o"></i>
12 days ago								 </span>
							</div>

						</div>

					</div>
				</div>

				<!-- Listing Item / End -->


			</div>
		</div>		<!-- Carousel / End -->

	</div>
</div>
<!-- Featured / End -->


<!-- Flip banner -->
<a href="listings-half-map-grid-standard.html" class="flip-banner parallax" data-background="images/flip-bg.jpg" data-color="#274abb" data-color-opacity="0.7" data-img-width="2500" data-img-height="1600">
	<div class="flip-banner-content">
		<h2 class="flip-visible">We help people and homes find each other</h2>
		<h2 class="flip-hidden">Browse Properties <i class="sl sl-icon-arrow-right"></i></h2>
	</div>
</a>
<!-- Flip banner / End -->



<!-- Footer
================================================== -->
<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo" src="images/official logo.jpg" alt="">
				<br><br>
				<p>Our mission is to be the leaders in the property market through excellent service and provision of a diverse range of realty products and services to the satisfaction of all our stakeholders</p>
		  </div>

			<div class="col-md-4 col-sm-6 ">
				<h4>Helpful Links</h4>
				<ul class="footer-links">
					<li><a href="login-register.php">Login</a></li>
					<li><a href="login-register2.php">Sign Up</a></li>
					<li><a href="login-register.php">My Account</a></li>


				</ul>

				<ul class="footer-links">
					<li><a href="#">FAQ</a></li>

					<li><a href="contact1.php">Contact</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
					<span>134 King George Ave, Avondale, Harare</span> <br>
Phone: <span>+263 4 304496 </span><br>
E-Mail:<span> <a href="#">sale@propertyexchange.co.zw</a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="https://www.facebook.com/www.propertyexchange.co.zw/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/propertyexchnge" target="_blank"><i class="icon-twitter"></i></a></li>


				</ul>

			</div>

		</div>

		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">� 2017 Property Exchange. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div><!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="scripts/rangeSlider.js"></script>
<script type="text/javascript" src="scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/masonry.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>




<!-- Style Switcher
================================================== -->
<script src="scripts/switcher.js"></script>
<!-- Style Switcher / End -->


</div>
<!-- Wrapper / End -->


</body>
</html>