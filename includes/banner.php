<div class="col-md-12">
				
				<div class="search-container">

					<!-- Form -->
					<h2>Find a New Home</h2>

					<!-- Row With Forms -->
				  <div class="row with-forms">

						<!-- Property Type -->
						<div class="col-md-3">
                         <form autocomplete="off" action="search_results.php" method="post">
							<select data-placeholder="Any Type" class="chosen-select-no-single"  name="type">
								<option>Houses</option>
								<option>Flats</option>
								<option>Commercial</option>
								<option>Stands</option>
								<option>Plots</option>
							</select>
						</div>

						<!-- Status -->
						<div class="col-md-3">
							<select data-placeholder="Any Status" class="chosen-select-no-single" name="status" >	
								<option>For Sale</option>
								<option>For Rent</option>
							</select>
						</div>

						<!-- Main Search Input -->
						<div class="col-md-6">
							<div class="main-search-input">
                          
								<input name="course" id="course" type="text" placeholder="Enter address e.g. province, city or suburb" value=""/>
								<button class="button"><i class="fa fa-search"></i></button>
                               
                                
							</div>
						</div>

					</div>
					<!-- Row With Forms / End -->

					<!-- Browse Jobs -->
                    
                    <div class="range-slider">
						<label>Price Range</label>
						<div id="price-range" data-min="0" data-max="3000000" data-unit="$" name="price"></div>
						<div class="clearfix"></div>
					</div>
					<div class="adv-search-btn range-slider">
					 <a href="listings-list-full-width.html">Advanced Search</a>
                        </div>
                        
 </form>
				</div>

			</div>