<div class="col-md-4">
			<div class="sidebar left">

				<div class="my-account-nav-container">
					
					<ul class="my-account-nav">
						<li class="sub-nav-title">Manage Account</li>
						<li><a href="my-profile.html" class="current"><i class="sl sl-icon-user"></i> My Profile</a></li>
                        <li><a href="view-users.php"><i class="sl sl-icon-action-redo"></i> View Users</a></li>
						
					</ul>
					
					<ul class="my-account-nav">
						<li class="sub-nav-title">Manage Listings</li>
						<li><a href="view-properties.php"><i class="sl sl-icon-action-redo"></i> My Properties</a></li>
						<li><a href="submit-property.php"><i class="sl sl-icon-action-redo"></i> Submit New Property</a></li>
					</ul>
                    
                    <ul class="my-account-nav">
						<li class="sub-nav-title">Manage Pages</li>
						<li><a href="https://www.cushycms.com/en/sites" target="_blank"><i class="sl sl-icon-action-redo"></i> About</a></li>
						<li><a href="https://www.cushycms.com/en/sites" target="_blank"><i class="sl sl-icon-action-redo"></i> Developments</a></li>
                        <li><a href="https://www.cushycms.com/en/sites" target="_blank"><i class="sl sl-icon-action-redo"></i>Showdays</a></li>
                        <li><a href="view-contacts.php"><i class="sl sl-icon-action-redo"></i> Contacts</a></li>
					</ul>
                    
                    
                    <ul class="my-account-nav">
						<li class="sub-nav-title">Locations</li>
						<li><a href="add_region.php"><i class="sl sl-icon-action-redo"></i>Add Location</a></li>
						<li><a href="add_suburb.php"><i class="sl sl-icon-action-redo"></i>Add Suburb</a></li>
					</ul>
                    
                     <ul class="my-account-nav">
						<li class="sub-nav-title">Categories (For rent, for sale etc)</li>
						<li><a href="add_category.php"><i class="sl sl-icon-action-redo"></i>Add Category</a></li>
						<li><a href="add_subcategory.php"><i class="sl sl-icon-action-redo"></i>Add Sub-category</a></li>
					</ul>
                    

					<ul class="my-account-nav">
						<li><a href="#"><i class="sl sl-icon-lock"></i> Change Password</a></li>
						<li><a href="logout.php"><i class="sl sl-icon-power"></i> Log Out</a></li>
					</ul>

				</div>

			</div>
		</div>