<?php

require('includes/config.php');
?>
<div class="col-md-12">
			<div class="carousel">
<?php
				$query="select * from book inner join subcat on b_subcat = subcat_id ORDER BY b_id DESC ";


				$res=mysqli_query($conn,$query);

				while($row=mysqli_fetch_assoc($res))
				{
				//echo "<option>".$row['cat_nm'];
					//var_dump($row);
					?>
				<!-- Listing Item -->
					<div class="carousel-item">
					<div class="listing-item">

						<a href="single-property-page.php?id=<?php echo $row['b_id'];?>" class="listing-img-container">

							<div class="listing-badges">
								<span class="featured">Featured</span>
								<span><?php echo $row['subcat_nm'];?></span>
							</div>

							<div class="listing-img-content">
								<span class="listing-price">$<?php echo $row['b_price'];
									if($row['subcat_id'] ==2 ||$row['subcat_id'] ==4||$row['subcat_id'] ==7)
									echo ' monthly';
									?></i></span>
								<span class="like-icon tooltip"></span>
							</div>

							<div class="listing-carousel">
								<div><img src="upload_image/<?php echo $row['b_id'].'-1-'.$row['b_img'];?>" alt="" ></div>
								<?php
							if (!empty($row['b_img2'])){
								?>
								<div><img src="upload_image/<?php echo $row['b_id'].'-2-'.$row['b_img2'];?>" alt=""></div>

								<?php } ?><!--<div><img src="upload_image/<?php echo $row['b_img'];?>" alt="" width="520" height="397"></div>-->
							</div>

						</a>
						
						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="single-property-page.php?id=<?php echo $row['b_id'];?>"><?php echo $row['b_edition']." ".$row['b_publisher'];?></a></h4>
								
							</div>

							<ul class="listing-features">
								<li>Area <span><?php echo $row['b_area']?> sq m</span></li>
								<li><i class="fa fa-bed" aria-hidden="true"></i>
 <span><?php echo $row['b_isbn'];?> </span></li>
								<li><i class="fa fa-bathtub"></i>
<span><?php echo $row['b_page'];?></span></li>
							</ul>

							<div class="listing-footer">
								<a href="single-property-page.php?id=<?php echo $row['b_id'];?>"><i class="fa fa-user"></i> <?php echo $row['b_user']?></a>
								<span><i class="fa fa-calendar-o"></i>
								<?php
								$zuva=explode(' ',$row['b_date']);
								list($yy,$mm,$dd)=explode('-',$zuva[0]);

								$start=date($yy.'-'.$mm.'-'.$dd);
								$end =date("Y-m-d");


								$tZone = new DateTimeZone('GMT');
								$dt1 = new DateTime($start, $tZone);
								$dt2 = new DateTime($end, $tZone);
								$ts1 = $dt1->format('Y-m-d');
								$ts2 = $dt2->format('Y-m-d');
								$diff = abs(strtotime($ts1)-strtotime($ts2));
								$diff/= 3600*24;

								echo $diff." days ago";
								?>
								 </span>
							</div>

						</div>

					</div>
				</div>

				<!-- Listing Item / End -->
              <?php
					}?>


			</div>
		</div>